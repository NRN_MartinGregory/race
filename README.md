# Atlassian Addon using React's create-react-app with Atlassian Connect Express

This is an **EXPERIMENTAL** "starter project" for creating a React-based addon using the standard frameworks provided by  [Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express) (ACE) and [`create-react-app`](https://github.com/facebookincubator/create-react-app), using the funkiness outlined by [this article](https://www.fullstackreact.com/articles/using-create-react-app-with-a-server/) on how to do React with an API server, but also enabling the "setup a tunnel and register with Cloud" funkiness of ACE.  

It provides a configuration where ACE serves all web requests, but proxies React requests to the create-react-app hotloading webserver.

It uses an **EXPERIMENTAL** version of ACE that supports a catchall route definition. 

##To use this project

- Set up a Jira Cloud Developer instance (https://developer.atlassian.com/cloud/jira/software/getting-started/)
- git clone https://NRN_MartinGregory@bitbucket.org/NRN_MartinGregory/race.git my-addon
- cd my-addon
- yarn install
- create credentials.json from credentials.json.template, and fill in correctly
- cd client
- yarn install
- cd ..
- npm start
- Browse to your Jira Cloud Dev instance, click "Hello World", and see "Welcome to React" served by your Dev React sever via your ACE server
- Browse to localhost:3001/api and see your test API being served by your API server

####Potential issues under investigation

- Is the serving of web-hooks or other ACE magic through the proxy messing up API serving?

Any input on this very welcome!  https://community.developer.atlassian.com/t/react-atlassian-connect-express/10361
 
----

#How this project was created

1 Basic ACE installation

- npm i -g atlas-connect
- atlas-connect new myaddon
- (select "jira")
- cd myaddon
- yarn install
- replace atlassian-connect-express with the experimental version, from https://bitbucket.org/NRN_MartinGregory/atlassian-connect-express.git
- create credentials.json, based on credentials.json.sample, check that it is in .gitignore
- node app.js
- Browse to you Jira Cloud app, click on Hello World and see the Hello World page being server by ACE
- kill node

2 Basic React Dev server

- npm i -g create-react-app 
- create-react-app client
- cd client
- yarn start
- See Welcome To React in your browser (at localhost:3000, it usually loads automatically)
- kill yarn
- cd ..

3 Set up scripting to run both servers, as per the funky link above

- change port to 3001 in config.json (because React webserver uses 3000)
- yarn add express-http-proxy
- yarn add concurrently --dev
- change package.json scripts to:

````javascript
  "scripts": {
    "start": "concurrently \"npm run server\" \"npm run client\"",
    "server": "AC_OPTS=force-reg node app.js",
    "client": "node start-client.js"
  },
````

- create start-client.js:

````
// cross platform way to do npm start in the client directory
const args = [ 'start' ];
const opts = { stdio: 'inherit', cwd: 'client', shell: true };
require('child_process').spawn('npm', args, opts);
````

- npm start
- See the Welcome To React page in your browser server by React Dev server (at localhost:3000, it usually loads automatically)
- Browse to you Jira Cloud app, click on Hello World and see the Hello World page being server by ACE
- kill npm

Both servers now start up OK.

4 Proxy React view routes to the React dev server, enable ACE javascript magic

- Put in place a catchall-routes.js:

####catchall-routes.js
````javascript
var proxy = require('express-http-proxy');

module.exports = function set_react_catchall_routes(app) {
    /* Final route to send anything else to react server. */
    console.log("Setting up catchall routes to react")
    app.use(proxy('localhost:3000'));
}
````

- Add Atlassian Connect scripting to the React index view:

####client/public/index.html
````
<!doctype html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.17/css/aui.min.css" media="all">
    <script src="https://ac-getting-started.atlassian.net/atlassian-connect/all.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="//aui-cdn.atlassian.com/aui-adg/5.9.17/js/aui.min.js"></script>
````

- npm start
- Browse to you Jira Cloud app, click on Hello World and this time see the Welcome To React served as a Jira page

(note that unfortuntely create-react-app magic opens a local browser page to the web dev server also - this is more of a nuisance than a help, but is baked into `create-react-app`)